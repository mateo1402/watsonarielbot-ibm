PROCESO=`ps -efa | grep watson_telegram.py | grep -v grep`
if [ -z "$PROCESO" ]
then
    cd /DATA/AI/bot/
    source /DATA/AI/bot/env_bot/bin/activate
    cd /DATA/AI/bot/
    python ./watson_telegram.py >> ./watson_telegram.log 2>&1 &
fi
