#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import json
import logging
import telegram
import threading
import string
import random
import ConfigParser, urllib2, MySQLdb
import ffmpeg
from watson_developer_cloud import VisualRecognitionV3
from MySQLdb import OperationalError
from datetime import datetime
#from lotes import LoteIngreso, LoteReserva, id_from_readable, stock_from_readable
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram import ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, Filters
from telegram.ext import RegexHandler,ConversationHandler, MessageHandler
from time import sleep
from os.path import join, dirname
from watson_developer_cloud import SpeechToTextV1
from watson_developer_cloud.websocket import RecognizeCallback


#Esta conf es para ver de forma general el log de la libreria.
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)
#This is the right way of setting logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('watson_telegram.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

#Menus
profile_keyboard = [[telegram.KeyboardButton('Actualizar Perfil')]]
custom_keyboard = [[telegram.KeyboardButton('Enviar Foto Rostro')]
                   ,[telegram.KeyboardButton('Enviar Audio Castellano')]]
                   #[telegram.KeyboardButton('Actualizar Perfil')],
                   # [telegram.KeyboardButton('Consultas')]]
nobody_keyboard = [[telegram.KeyboardButton('/start')]]
admin_keyboard =  [[telegram.KeyboardButton('Enviar Foto Rostro')]
                   ,[telegram.KeyboardButton('Enviar Audio Castellano')]]
                   #[telegram.KeyboardButton('Actualizar Perfil')],
                   # [telegram.KeyboardButton('Consultas')]]

#Constantes
SIN_FOTO = 'sin_foto.jpg'
DATOS_RESERVA = 'Usted ha reservado:\nProducto: %s\nCantidad:%d\n'
GRACIAS_RESERVA = 'NRO. RESERVA: %d\nID Producto: %d\nGracias! Tu reserva está confirmada! Serás contactado para continuar con la operación.'
ERROR_RESERVA_24 = 'Error al intentar reservar. Recuerde que los productos están disponibles por 24hs.'
ERROR_RESERVA_STOCK = 'Error al intentar reservar. No hay mas stock disponible.'
ERROR_RESERVA = 'Error al intentar reservar.'
NO_PRODUCTS = 'No hay productos nuevos ingresados'
NO_RESERVAS = 'No hay reservas del ultimo día'
NOT_LOGGED_MSG = 'Hola! Para realizar operaciones debes indicar tu id al Administrador. Utiliza el comando /miid para saber tu id.'
BANNED_MSG = 'USER HAS BEEN BANNED'
SALUDO_EXPLICACION_SIN_PERFIL = '''Hola! Bienvenido a PROBAR WATSON de IBM.Por Favor actualice su perfil.'''
SALUDO_EXPLICACION_CON_PERFIL = '''Hola! Bienvenido a PROBAR WATSON de IBM. Aquí podrá probar enviar una foto y que Watson de IBM le reconozca sexo y edad como enviar un audio y le devuelva el texto del mismo.'''
NOMBRE_MSG = 'Por favor, ingrese el nombre identificador del producto.'
INGRESO_MSG = 'Excelente, ha comenzado la carga del producto. Puede cancelar en cualquier momento haciendo click en /cancelar_producto. A continuación, ingrese la descripción del producto.'
PHOTO_MSG = 'Por favor, ingrese una imagen de una cara.Puede cancelar en cualquier momento haciendo click en /cancelar_foto_cara'
CANTIDAD_POR_LOTE_MSG = 'Ingrese la cantidad de unidades por lote, con numeros'
CANTIDAD_MSG = 'Ingrese la cantidad de unidades disponibles.'
PRECIO_MSG = 'Ingrese precio por unidad en USDT'
PRECIO_ERROR = 'El formato del precio NO es el esperado. No incluya moneda. Ingrese el precio en USDT, por ejemplo: 39,99'
CANTIDAD_ERROR = 'El formato de la cantidad NO es el esperado. Por favor ingrese solo números.'
GRACIAS_INGRESO = 'Muchas Gracias. Su ingreso será procesado a la brevedad.'
ACTUALIZAR_PERFIL = 'Para continuar, por favor, actualice su perfil'
GRACIAS_INGRESO_PERFIL = 'Muchas Gracias por actualizar su perfil.'
ERROR_INGRESO = 'El ingreso del producto no pudo ser procesado. Por favor intente nuevamente.'
ERROR_INGRESO_PERFIL = 'El ingreso del perfil no pudo ser procesado. Por favor intente nuevamente.'
INGRESO_PERFIL_MSG = 'Excelente, ha comenzado la carga de su perfil. Puede cancelar en cualquier momento haciendo click en /cancelar_perfil. A continuación, ingrese su nombre.'
PAIS_MSG = 'Ingrese su país de residencia' 
PROVINCIA_MSG = 'Ingrese su provicia o estado de residencia'
PHOTO = 1
NOMBRE, PAIS, PROVINCIA = range(3)
CONSULTA = 1
AUDIO = 1
AGREGAR_USUARIO_OK = 'El usuario ha sido agregado exitosamente.'
AGREGAR_USUARIO_ERROR = 'Error al intentar agregar al usuario.'
SALUDO_SIN_ALIAS = 'Para operar debes ingresar un alias de telegram y luego aprieta el boton /start'
UTILICE_TECLADO = 'Pro favor, utilice la botonera para publicar productos.'
INGRESO_CONSULTA = 'Ha comenzado el ingreso de su consulta (Por favor, hagalo en una sola frase) y presione Enviar. Puede cancelar en cualquier momento el ingreso de su consulta haciendo click en /cancelar_consulta'
GRACIAS_INGRESO_CONSULTA = 'Hemos recibido tu consulta. Te la responderemos a la brevedad por este mismo canal.'
ERROR_INGRESO_CONSULTA = 'Su consulta no pudo ser ingresada.'
TIENE_CONSULTA_PENDIENTE = 'Tenemos una consulta previa en estado Pendiente de Responder. Podrás enviar una nueva consulta cuando hayamos respondido la consulta pendiente.'
ESPERO_FOTO = 'Espero haberle acertado a tu edad y sexo.'
ESPERO_AUDIO = 'Espero haberle acertado la conversión de speech to text.'
AUDIO_MSG = 'Por favor, envíe un AUDIO.Puede cancelar en cualquier momento haciendo click en /cancelar_audio'

# Global
producto = {}
perfil = {} 
consulta = {}
#utils
def get_random_string(N=8):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))

# Funciones de DB
def fetch_data(query):
    global config
    try:
        db = MySQLdb.connect(host=config.get('Db','Host'), port=int(config.get('Db','Port')) , user=config.get('Db','Usuario'), passwd=config.get('Db','Contrasena'), db=config.get('Db','Db'))
        db.autocommit(1)
        cursor=db.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)
        data=cursor.fetchall()
        cursor.close()
        db.close()
        return data
    except OperationalError as e:
        #reconnect()
        logger.error( 'MySQL Exception, trying again. %s ' % str(e) )
        fetch_data(query)

def response_consultas():
    while True:
        query="select CAST(CONVERT(c.texto_consulta USING utf8) AS BINARY) as texto_consulta, CAST(CONVERT(c.texto_respuesta USING utf8) AS BINARY) as texto_respuesta, push_notif_only, CAST(CONVERT(n.first_name USING utf8) AS BINARY) as first_name, c.telegram_mobile, c.id  from T_TELEGRAM_CONSULTAS c join T_TELEGRAM_LOG_NOMBRE n on (c.telegram_mobile=n.mobile) where estado_consulta='UPDATEINGUSER';"
        respuestas = fetch_data(query)
        for respuesta in respuestas:
            if respuesta['push_notif_only']==0:
                texto="Hola <b>"+str(respuesta['first_name'].title())+"\nConsulta</b>: "+str(respuesta['texto_consulta'])+"\n<b>Respuesta:</b> "+str(respuesta['texto_respuesta'])+"\nSaludos y buenas ventas !\n"
            else:
                texto="Hola <b>"+str(respuesta['first_name'].title())+"</b>. "+str(respuesta['texto_respuesta'])+"\nSaludos y buenas Ventas!\n"
            reply_markup = keyboard_correspondiente(respuesta['telegram_mobile'])
            bot.sendMessage(chat_id=respuesta['telegram_mobile'] ,text=texto , reply_markup=reply_markup, parse_mode="HTML")
            query="update T_TELEGRAM_CONSULTAS set estado_consulta='CLOSED' where id="+str(respuesta['id'])
            fetch_data(query)

        sleep(random.randint(1, 3))

def alta_usuario(miid):
    try:
        query='''INSERT into T_TELEGRAM_ALLOWED_WORDS (mobile, word) values (%s, 'ingresar_producto'), (%s, 'start');''' % (miid, miid, miid)
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def permission(command, chat_id):
    query="select * from T_TELEGRAM_ALLOWED_WORDS where word='%(word)s' and mobile = %(chat_id)s ;" % {'word':command, 'chat_id' : chat_id}
    results=fetch_data(query)
    if len(results) > 0:
        return True
    return False

def productos(availability):
    try:
        query='''SELECT *  FROM T_TELEGRAM_PRODUCTOS where
        boAvailability=%(boAvailability)d
        ;''' % {'boAvailability': availability}
        #logger.debug(query)
        return fetch_data(query)
    except Exception as e:
        logger.error(str(e))

def telegram_log(telegram_mensaje, telegram_from, username, first_name):
    query="insert into T_TELEGRAM_LOG set mobile="+str(telegram_from)+", texto='"+str(telegram_mensaje)+"', mobile_listener='"+str(config.get('Telegram','robot'))+"', datetime=now(); insert into T_TELEGRAM_LOG_NOMBRE set mobile="+str(telegram_from)+", first_name='"+str(first_name)+"', username='"+str(username)+"'  ON DUPLICATE KEY UPDATE first_name='"+str(first_name)+"' , username='"+str(username)+"' ;"
    fetch_data(query)
    if username is None:
        query="delete from T_TELEGRAM_ALLOWED_WORDS where mobile="+str(telegram_from)+" and word='alias';"
    else:    
        query="insert ignore into T_TELEGRAM_ALLOWED_WORDS set mobile="+str(telegram_from)+",word='alias';"
    fetch_data(query)

def telegram_ingreso_foto_cara(producto,mobile):
    global config
    try:
        foto = producto['foto']
  
        #Watson detección de cara 
        fphoto = str(config.get('Pics','Url'))
        fphoto += str(producto['foto'])
        visual_recognition = VisualRecognitionV3(str(config.get('Watson','Version')),iam_api_key=str(config.get('Watson','Api_Key')))
        url_imagen= fphoto  
        classes = visual_recognition.detect_faces(url=url_imagen,accept_language="es")
        sexo = json.dumps(classes["images"][0]["faces"][0]["gender"]["gender"])
        min_edad = json.dumps(classes["images"][0]["faces"][0]["age"]["min"])
        max_edad = json.dumps(classes["images"][0]["faces"][0]["age"]["max"])
        texto = "Sexo: " + str(sexo) +"\nMin_Edad: " + str(min_edad)
        texto += "\nmax_edad: " + str(max_edad)
        reply_markup = keyboard_correspondiente(mobile)
        bot.sendMessage(chat_id=mobile ,text=texto , reply_markup=reply_markup, parse_mode='HTML')

        query="INSERT into T_TELEGRAM_PRODUCTOS set mobile='"+str(mobile)+"', nuPrecio=0, nuCantidad=0, dsDescription='', dtFecha=now() , dsImg='"+str(foto)+"';"
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def telegram_ingreso_audio(producto,mobile):
    global config
    try:
        speech_to_text = SpeechToTextV1(
                      username=str(config.get('Watson','Username')),
                      password=str(config.get('Watson','Password')),
                      url=str(config.get('Watson','Url')))
        audio = producto['audio']

        #Watson voice to text 
        avoice = str(config.get('Pics','Url'))
        avoice += str(producto['audio'])
        print ("voice...............................: ",avoice)
        with open(join(dirname(__file__), str(config.get('Pics','Path'))+str(producto['audio'])),'rb') as audio_file:
            texto = json.dumps(speech_to_text.recognize(
                audio=audio_file,
                content_type='audio/wav',
                timestamps=True,model="es-ES_BroadbandModel",
                word_confidence=True)['results'][0]['alternatives'][0]['transcript'],indent=2,ensure_ascii=False)
        reply_markup = keyboard_correspondiente(mobile)
        bot.sendMessage(chat_id=mobile ,text=texto , reply_markup=reply_markup, parse_mode='HTML')

        query="INSERT into T_TELEGRAM_PRODUCTOS set mobile='"+str(mobile)+"', nuPrecio=0, nuCantidad=0, dsDescription='', dtFecha=now() , dsImg='"+str(audio)+"';"
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False


def telegram_ingreso_consulta(consulta,mobile):
    try:
        pregunta = consulta['consulta']
        query="INSERT into T_TELEGRAM_CONSULTAS set telegram_mobile='"+str(mobile)+"', fecha_consulta=now(), estado_consulta='OPEN', texto_consulta='"+str(pregunta)+"';"
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def telegram_ingreso_perfil(perfil,mobile):
    try:
        nombre = perfil['nombre']
        pais = perfil['pais']
        provincia = perfil['provincia']
        query="INSERT into T_TELEGRAM_LOG_NOMBRE set dsName='"+str(nombre)+"', dsCountry='"+str(pais)+"', dsProvince='"+str(provincia)+"', mobile='"+str(mobile)+"' ON DUPLICATE KEY UPDATE dsName='"+str(nombre)+"', dsCountry='"+str(pais)+"', dsProvince='"+str(provincia)+"';"
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def cara(bot, update):
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    if not valid_perfil(update.message.chat_id):
        # Respuesta para cuando el user tiene alias pero no tiene completo el perfil
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(ACTUALIZAR_PERFIL,reply_markup=reply_markup)
        return
    update.message.reply_text(PHOTO_MSG,reply_markup=keyboard_correspondiente(update.message.chat_id) )
    producto[update.message.chat_id] = {} 
    return PHOTO

def profile(bot, update):
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    update.message.reply_text(INGRESO_PERFIL_MSG,reply_markup=keyboard_correspondiente(update.message.chat_id) )
    perfil[update.message.chat_id] = {} 
    return NOMBRE

def question(bot, update):
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    query="select count(*) as consultas from T_TELEGRAM_CONSULTAS where estado_consulta in ('OPEN','WAITINGFORRESPONSE') and telegram_mobile="+str(update.message.chat_id)
    consultas_pending=fetch_data(query)
    if consultas_pending[0]['consultas']!=0:
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(TIENE_CONSULTA_PENDIENTE,reply_markup=reply_markup)
        return
    update.message.reply_text(INGRESO_CONSULTA,reply_markup=keyboard_correspondiente(update.message.chat_id) )
    consulta[update.message.chat_id] = {} 
    return CONSULTA

def timeout(bot, update):
    logger.error("User %s timeout the conversation." % user.first_name)
    update.message.reply_text('Se ha superado el tiempo limite para el ingreso del producto, por favor, comience la operación nuevamente',
                              reply_markup=keyboard_correspondiente(update.message.chat_id))
    if update.message.chat_id in ingresos:
        del ingresos[update.message.chat_id]
    return ConversationHandler.END

def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the product input." % user.username)
    update.message.reply_text('Se ha cancelado el ingreso del producto, por favor, comience la operación nuevamente',
                              reply_markup=keyboard_correspondiente(update.message.chat_id))
    if update.message.chat_id in producto:
        del producto[update.message.chat_id]
    return ConversationHandler.END

def cancel_profile(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the profile input." % user.username)
    update.message.reply_text('Se ha cancelado el ingreso del perfil, por favor, comience la operación nuevamente',
                              reply_markup=keyboard_correspondiente(update.message.chat_id))
    if update.message.chat_id in ingresos:
        del ingresos[update.message.chat_id]
    return ConversationHandler.END

def cancel_question(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the question input." % user.username)
    update.message.reply_text('Se ha cancelado el ingreso de la consulta, por favor, comience la operación nuevamente',
                              reply_markup=keyboard_correspondiente(update.message.chat_id))
    if update.message.chat_id in consulta:
        del consulta[update.message.chat_id]
    return ConversationHandler.END

def input_question(bot, update):
    user = update.message.from_user
    if update.message.chat_id in consulta:
        consulta[update.message.chat_id]['consulta'] = update.message.text
    else:
        cancel_question(bot, update)
    logger.info("El la consulta enviada por %s: %s" % (user.username, update.message.text))
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    if telegram_ingreso_consulta(consulta[update.message.chat_id],update.message.chat_id):
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(GRACIAS_INGRESO_CONSULTA, reply_markup=reply_markup)
    else:
        update.message.reply_text(ERROR_INGRESO_CONSULTA, reply_markup=reply_markup)

    if update.message.chat_id in consulta:
        del consulta[update.message.chat_id]
    return ConversationHandler.END

def nombre_perfil(bot, update):
    user = update.message.from_user
    if update.message.chat_id in perfil:
        perfil[update.message.chat_id]['nombre'] = update.message.text
    else:
        cancel_profile(bot, update)
    logger.info("El nombre de perfil enviado por %s: %s" % (user.username, update.message.text))
    update.message.reply_text(PAIS_MSG)
    return PAIS

def pais_perfil(bot, update):
    user = update.message.from_user
    if update.message.chat_id in perfil:
        perfil[update.message.chat_id]['pais'] = update.message.text
    else:
        cancel_profile(bot, update)
    logger.info("El pais de perfil enviado por %s: %s" % (user.username, update.message.text))
    update.message.reply_text(PROVINCIA_MSG)
    return PROVINCIA

def provincia_perfil(bot, update):
    user = update.message.from_user
    if update.message.chat_id in perfil:
        perfil[update.message.chat_id]['provincia'] = update.message.text
    else:
        cancel(bot, update)
    logger.info("la provincia de perfil enviado por %s: %s" % (user.username, update.message.text))
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    if telegram_ingreso_perfil(perfil[update.message.chat_id],update.message.chat_id):
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(GRACIAS_INGRESO_PERFIL, reply_markup=reply_markup)
    else:
        update.message.reply_text(ERROR_INGRESO_PERFIL, reply_markup=reply_markup)

    if update.message.chat_id in perfil:
        del perfil[update.message.chat_id]
    return ConversationHandler.END

def audio(bot, update):
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    if not valid_perfil(update.message.chat_id):
        # Respuesta para cuando el user tiene alias pero no tiene completo el perfil
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(ACTUALIZAR_PERFIL,reply_markup=reply_markup)
        return
    update.message.reply_text(AUDIO_MSG,reply_markup=keyboard_correspondiente(update.message.chat_id) )
    producto[update.message.chat_id] = {}
    return AUDIO


def audio_text(bot, update):
    import os
    global config
    user = update.message.from_user
    audio_file = bot.getFile(update.message.voice.file_id)
    random = get_random_string()
    audioname = random + '.ogg'
    audiofile = config.get('Pics','Path') + audioname
    audio_file.download(audiofile)
    os.system('avconv -i '+str(config.get('Pics','Path'))+str(audioname)+' -c:a pcm_f32le '+str(config.get('Pics','Path')) +str(random)+'.wav')
    os.system('rm '+str(config.get('Pics','Path'))+str(audioname))
    audioname = str(random)+'.wav'
    if update.message.chat_id in producto:
        producto[update.message.chat_id]['audio'] = random + '.wav'
    else:
        cancel(bot, update)

    logger.info("El audio fue enviado por %s: %s" % (user.username, audioname))
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    if telegram_ingreso_audio(producto[update.message.chat_id],update.message.chat_id):
        update.message.reply_text(ESPERO_AUDIO, reply_markup=reply_markup)
    else:
        update.message.reply_text(ERROR_INGRESO, reply_markup=reply_markup)

    if update.message.chat_id in producto:
        del producto[update.message.chat_id]
    return ConversationHandler.END


def foto_cara(bot, update):
    global config
    user = update.message.from_user
    photo_file = bot.getFile(update.message.photo[-1].file_id)
    photoname = get_random_string() + '.jpg'
    photofile = config.get('Pics','Path') + photoname
    photo_file.download(photofile)
    if update.message.chat_id in producto:
        producto[update.message.chat_id]['foto'] = photoname
    else:
        cancel(bot, update)
 
    logger.info("La photo enviada por %s: %s" % (user.username, photoname))
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    if telegram_ingreso_foto_cara(producto[update.message.chat_id],update.message.chat_id):
        update.message.reply_text(ESPERO_FOTO, reply_markup=reply_markup)
    else:
        update.message.reply_text(ERROR_INGRESO, reply_markup=reply_markup)

    if update.message.chat_id in producto:
        del producto[update.message.chat_id]
    return ConversationHandler.END

def valid_perfil(mobile):
    query = "select IF(dsName is NULL or dsCountry is NULL or dsProvince is NULL,0,1) as valid_profile from T_TELEGRAM_LOG_NOMBRE where mobile='"+str(mobile)+"';"
    results = fetch_data(query)
    if len(results) > 0:
        if results[0]['valid_profile'] == 0 :
            return False
        else:
            return True


def keyboard_correspondiente(chat_id):
    keyboard = []
    #
    if permission('admin', chat_id):
        # Admin
        keyboard = admin_keyboard
    elif not permission('alias', chat_id):
        # Respuesta para start cuando no se conoce el usuario
        keyboard = nobody_keyboard
    else:
        # Respuesta si no tiene perfil actualizado o si tiene ya puede operar
        if not valid_perfil(chat_id): 
            keyboard = profile_keyboard
        else:
            keyboard = custom_keyboard

    return ReplyKeyboardMarkup(keyboard, resize_keyboard=True,
                                       one_time_keyboard=False)

def cuantos(bot, update):
    mobile=update.message.chat_id
    telegram_log(update.message.text, update.message.chat_id, update.message.chat['username'], update.message.chat['first_name'])
    if permission('cuantos', mobile) and permission('admin', mobile) and not permission('banned', mobile):
        query="select count(distinct mobile) as total_usuarios from T_TELEGRAM_ALLOWED_WORDS;"
        cuantos=fetch_data(query)[0]
        texto="<b>"+str(cuantos['total_usuarios'])+"</b> usuarios"

        query="select count(*) as total from T_TELEGRAM_CONSULTAS where estado_consulta in ('WAITINGFORRESPONSE','OPEN');"
        consultas=fetch_data(query)[0]
        if len(consultas)>0:
            if consultas['total']>0:
                texto+="\n<b>"+str(consultas['total'])+"</b> consultas pendientes"

        query="select count(*) as total, case boAvailability when 0 then 'Pendiente Aprobacion Admins' when 1 then 'Publicados' when 2 then 'Rechazados' END  as estado from T_TELEGRAM_PRODUCTOS group by boAvailability;"
        productos=fetch_data(query)
        if len(productos)>0:
            for p in productos:
                texto+="\n<b>"+str(p['total'])+"</b> de productos "+str(p['estado'])
        #BoAvailability: Aceptado:1, Rechazado: 2, Pendiente de aprobacion admin: 0
        #boSent: pusheado al canal [0|1]
        #boAlert: producto alertado, o no, a admins para aprobacion [0|1]

        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(texto,reply_markup=reply_markup, parse_mode='HTML')
        
def miid(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    update.message.reply_text(update.message.chat_id)

def agregar_usuario(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if permission('admin', update.message.chat_id):
        rr = update.message.text.split(' ')[1]
        try:
            miid = int(rr)
            if alta_usuario(miid):
                update.message.reply_text(AGREGAR_USUARIO_OK)
            else:
                update.message.reply_text(AGREGAR_USUARIO_ERROR)
        except Exception as e:
            update.message.reply_text(AGREGAR_USUARIO_ERROR)
            return

def start(bot, update):
    #print(update.message.text)
    # Whois
    telegram_log(update.message.text, update.message.chat_id, update.message.chat['username'], update.message.chat['first_name'])

    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return

    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return

    if not valid_perfil(update.message.chat_id):
        # Respuesta para cuando el user tiene alias pero no tiene completo el perfil
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(ACTUALIZAR_PERFIL,reply_markup=reply_markup)
        return
 
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    update.message.reply_text(SALUDO_EXPLICACION_CON_PERFIL,reply_markup=reply_markup)

def help(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    update.message.reply_text("Ingrese /start para comenzar.")
    if permission('admin', update.message.chat_id):
        update.message.reply_text("Recuerde que para ingresar un nuevo usuario debe escribir: /agregar_usuario <miid>")

def error(bot, update, error):
    logging.warning('Update "%s" caused error "%s"' % (update, error))

def message(bot, update):
    logger.warning('Unexpected message')
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    if not valid_perfil(update.message.chat_id):
        # Respuesta para cuando el user tiene alias pero no tiene completo el perfil
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(ACTUALIZAR_PERFIL,reply_markup=reply_markup)
        return
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    update.message.reply_text(UTILICE_TECLADO, reply_markup=reply_markup)
        

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Read confir form file
config = ConfigParser.SafeConfigParser()
config.read('watson_telegram.ini')

# Create the Updater and pass it your bot's token.
token = str(config.get('Telegram','token'))
updater = Updater(token, workers=10)
updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(CommandHandler('miid', miid))
updater.dispatcher.add_handler(CommandHandler('cuantos', cuantos))
#updater.dispatcher.add_handler(CommandHandler('agregar_usuario', agregar_usuario))
updater.dispatcher.add_handler(CommandHandler('help', help))
updater.dispatcher.add_error_handler(error)

# Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
conv_handler_rostro = ConversationHandler(
    entry_points=[RegexHandler('Enviar Foto Rostro', cara)],

    states={
        PHOTO: [MessageHandler(Filters.photo, foto_cara)]
    },

    fallbacks=[CommandHandler('cancelar_foto_cara', cancel)],
    allow_reentry=True,
    run_async_timeout=1,
    timed_out_behavior=[CommandHandler('timeout', timeout)]
)

# Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
conv_handler_audio = ConversationHandler(
    entry_points=[RegexHandler('Enviar Audio Castellano', audio)],

    states={
        AUDIO: [MessageHandler(Filters.voice, audio_text)]
    },

    fallbacks=[CommandHandler('cancelar_audio', cancel)],
    allow_reentry=True,
    run_async_timeout=1,
    timed_out_behavior=[CommandHandler('timeout', timeout)]
)


# Add conversation handler with the states PROFILE, NAME, COUNTRY, PROVINCE
conv_handler_perfil = ConversationHandler(
    entry_points=[RegexHandler('Actualizar Perfil', profile)],

    states={
        NOMBRE: [MessageHandler(Filters.text, nombre_perfil )],
        PAIS: [MessageHandler(Filters.text, pais_perfil )],
        PROVINCIA: [MessageHandler(Filters.text, provincia_perfil )]
    },

    fallbacks=[CommandHandler('cancelar_perfil', cancel_profile)],
    allow_reentry=True,
    run_async_timeout=1,
    timed_out_behavior=[CommandHandler('timeout', timeout)]
)

# Add conversation handler CONSULTAS
conv_handler_consulta = ConversationHandler(
    entry_points=[RegexHandler('Consultas', question)],

    states={
         CONSULTA : [MessageHandler(Filters.text, input_question )]
    },

    fallbacks=[CommandHandler('cancelar_consulta', cancel_question)],
    allow_reentry=True,
    run_async_timeout=1,
    timed_out_behavior=[CommandHandler('timeout', timeout)]
)



updater.dispatcher.add_handler(conv_handler_rostro)
updater.dispatcher.add_handler(conv_handler_audio)
updater.dispatcher.add_handler(conv_handler_perfil)
updater.dispatcher.add_handler(conv_handler_consulta)
updater.dispatcher.add_handler(MessageHandler(Filters.all,message))

t = threading.Thread(name='response_consultas', target=response_consultas)
t.setDaemon(True)
t.start()

# Create the Updater and pass it your bot's token.
token = str(config.get('Telegram','token'))
bot = telegram.Bot(token=token)

# Start the Bot
updater.start_polling()


# Run the bot until the user presses Ctrl-C or the process receives SIGINT,
# SIGTERM or SIGABRT
updater.idle()
