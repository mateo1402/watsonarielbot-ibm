CREATE TABLE `T_TELEGRAM_CONSULTAS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telegram_mobile` bigint(20) NOT NULL,
  `fecha_consulta` datetime NOT NULL,
  `estado_consulta` varchar(20) NOT NULL,
  `texto_consulta` varchar(1000) NOT NULL,
  `texto_respuesta` varchar(1000) DEFAULT NULL,
  `fecha_respuesta` datetime DEFAULT NULL,
  `push_notif_only` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `consultas_mobile_idx` (`telegram_mobile`),
  KEY `estado_consulta_idx` (`estado_consulta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `T_TELEGRAM_LOG_NOMBRE` (
  `mobile` bigint(20) NOT NULL DEFAULT '0',
  `first_name` varchar(250) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `dsName` varchar(250) DEFAULT NULL,
  `dsCountry` varchar(250) DEFAULT NULL,
  `dsProvince` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`mobile`),
  KEY `log_nombre_idx_mobile` (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;


CREATE TABLE `T_TELEGRAM_PRODUCTOS` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` bigint(20) NOT NULL,
  `nuPrecio` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `nuCantidad` int(6) unsigned NOT NULL DEFAULT '0',
  `dsDescription` longtext CHARACTER SET latin1,
  `dtFecha` datetime DEFAULT NULL,
  `dsImg` varchar(140) DEFAULT NULL,
  `boAvailability` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `boSent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `boAlert` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2 ;


CREATE TABLE `T_TELEGRAM_MENSAJES` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idProduct` int(10) unsigned DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `idMsg` int(10) unsigned DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2 ;

CREATE TABLE `T_TELEGRAM_ALLOWED_WORDS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` bigint(20) NOT NULL,
  `word` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `T_TELEGRAM_LOG` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mobile` bigint(20) DEFAULT NULL,
  `texto` varchar(250) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mobile_listener` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `log_idx_mobile` (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


